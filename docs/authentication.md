# Authentication 

## Registration system

Laravel provides some tools that creates scafolding with a login, register and verify blade because this blades are something very common so Laravel automate this process so we don't have to write all the code. Also it important to mention that we can use a middleware to check if the user is actually logged in the app


## Password reset flow

1. User clicks "Forgot Password"
    
   First we have to activate the driver that if in the .env file

   ![Email](./images/maildriver.png "Email")

   Just replace the driver for the one that you'll be using on the website

2. Fill out a form with their email
3. Prepare unique token and associate it with the user's account
4. Send an email with a unique link that confirms email ownership
5. Link back to website, confirm the token, and set a new a password





