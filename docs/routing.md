# Routing

The first thing that happens when a request comes to our app is the controller, this the where the magic happens

Firts we load a controller, this the one in charge of going receive the request and give a response, also it is in charge of going to the differents files and ask for the differents resources need it to show the view or the data

## Requests in Laravel

Lets see how requests works in [Laravel](https://www.laravel.com)

In your [Laravel](https://www.laravel.com) app you will find a folder named routes 

![Routes folder](./images/Routes.png "Routes Folder")

Once we are there, we are gonna select the web.php file, in that file is where all the routes to the differents blades or views are registered

![Routes file](./images/Routes-file.png "Routes file")

Over there we can see that if someone send a request using get but without anything else but the domain, our app has to load the welcome view,
this means that if a user sends a request with a url that is no registered in this file so it will get an error because for our app that url or route doesn't exists

## Pass request data to views

We can use the request in laravel to pass different data, names, values, numbers but it is not a good practice if you use this method to pass usernames or passwords but we will talk about that in the future, but now let's see how we can use this method to pass data to views

For example: 

![Data Pass](./images/Datapass.png "Data pass")

We need to declare the data in a variable with the name that we are going to use, then when we are returning the view we have to make sure that we pass the different data to the view so we can use it over there

## Route Wildcards 

We can use this to make our app accepts different values that might be use in a database query, lets see how we can use this

![Wildcard](./images/Wildcard.png "Wildcard")

We are using "$posts" as a representation of a data base where we send the post selected to see if there is any information, but if the post is not in our data base so we say that the app should answer with a 404

## Routing to controllers

This a very usefull feature when we are talking about large size applications, so lets see how we can reload a controller here

![Redirect Controller](./images/redirectcontroller.png "Redirect")

As we see before, that is the correct way to redirect to a controller and calling the method show, now lets see what is inside of that controller

![Controller](./images/controller.png "Controller")

Inside of the controller we took the same logic that we used explaining the wildcards and now PostsController is in charge of takin the decision if shows the view or the error, and that is a better way of distribution than having a bunch of methods and logic in the routes file