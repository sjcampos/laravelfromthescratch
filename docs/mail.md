# Mail

## Send raw mail

The first thing to do is verify that the user is writing a valid email, we can make this verification in the front end and also on the backend.

### Validating email in the backend
Quick note this a validation in the controller, but if you are also developing a web api you need to make this validation over there to make sure that is a valid  email.

```
public function send()
{
    reques()->validate(['email'=> 'required|email']); //if this fails, laravel will never send the email

    //Here you should send an error message

    Mail::raw('It works',function($message)){
        $message->to(request('email'))
            ->subject('hello') //Here you take the subject from the request or you can have a default message
    }

    return redirect('/contact')
        ->with('message','Email sent!');
}
```
Note: You can change the 'from' mail in the .env file, just need to add the next code 

![Mail](./images/mail.png "Mail")

And write the email you want

## Mailable classes

We can use "php artisan make:email" to create an email class this will help us to send more complex emails and usign html code



