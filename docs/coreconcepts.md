# Core concepts

## CSRF attacks

Most of the users don't know what is a CSRF but Laravel actually cover that for us. Cross-site request forgery or CSRF is an attack where the attackers take advantage of a authorized session on a website to steal information, or just make damage to the app. Laravel automatically generates a CSRF "token" for each active user session managed by the application. This token is used to verify that the authenticated user is the one actually making the requests to the application.

## Service container fundamentals

Laravel offers a service container, this is a very powerful tool for managing class dependecies and performing dependecy injection. We can use containers to store or retrieve services and by using biding we can have access to those services.



