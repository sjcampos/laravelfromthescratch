# Notifications

## SMS notifications

[Laravel](https://laravel.com/) provides a sms notification powered by [Nexmo](https://developer.nexmo.com/),  you need to install the laravel/nexmo-notification-channel
```
composer require laravel/nexmo-notification-channel
```
To route Nexmo notifications to the proper phone number, define a routeNotificationForNexmo method on your notifiable entity:
```
<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * Route notifications for the Nexmo channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForNexmo($notification)
    {
        return $this->phone_number;
    }
}
```
We can see more information about notification right [here](https://laravel.com/docs/8.x/notifications)