# Forms

## The seven restful controller actions

There are 7 restfull controller actions that we should be aware of, lets se what are these methods

1- **Index()**  *Index should always returns a list of data, it does not matter if they are persons, articles, etc*

2- **Show()**  *Show is for getting or showing a specific data, for example one student, one article...*

3- **Create()** *Create shows a view to create a new resource*

4- **Store()** *Store persist the new resource*

5- **Edit()** *Edit shows a view to editing a resource*

6- **Update()** *Update persist the edited resource*

7- **Destroy()** *Destroy destroys a resource*

This is a convection that we should follow in each project so we know that we have at least the minium  required to make our app useful

## Restful routing

This concept is related to REST, and with this we are going to avoid verbs ir ours urls, for that  we can use this verbs ***GET, POST, PUT, DELETE*** that allows us to use a same url for diferrent purposes for example:
```
GET /article/:id
DELETE /article/:id
```
We are using the same url but with different verb so one will give us a specific article and the other will destroy that article, that is why this an important topic for us

## Form Validation

This is a very important method and let me tell you why, as a developer one of our main goals is to create safe apps for the users so it is our responsability to validate all the information before sending a request, we have to sure that everything is validated, from whitespace to null spaces, numbers, symbols, everything is important and we never have to forget about it. In [Laravel](https://laravel.com) we have a lot of documentation for this topic and even the framework integrates some options to helps us as a developers to validate information.

