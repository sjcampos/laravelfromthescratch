# Views

## Layout pages

Layout pages are use to write all the scripts, fonts links, styles links, doctype and all that information that we can reuse in another view so we do not have to go through all the diffents views adding that information and we will store all the content in our main blade, for that we will have to make that refence in the main blade using this
```
@extends ('name-of-your-layout')
```
Then we have to go to the layout file and specify where are we going to put all the html from the blade, and for that we go to the body of the layout and add the next sentence
```
<body>
    @yield('content')
</body>
```
After this we just have to go to our main view and create the section that we just yield and put all the html over there, so you will have a view that should look like this
```
@extends ('name-of-your-layout')

@section ('content')

HERE YOU ARE GONNA PASTE ALL YOUR HTML 

@endsection
```
And with this you will have the data apart from the scripts, css, fonts, etc.

## Integrate a site template

We all have been in the position where we want to use an external design or template for our app, for this we have to create a new directory in the public directory called 'css', here we are going to put all of the css files from the template so we can use them in our view, then we copy the images in the 'images directory' and for the last step we just have to use the same method that we use for the layout template.

IMPORTANT: Remember that you have to change all the links of the images, css files, etc, and use the new locations.

## Set an active menu link

This is a very easy but important topic, because most of times we want to redirect the users to differents websites or views, so we have to enable that option so lets see how you should do it
```
<ul>
    <li class="{{ Request::path()=== '/' ? 'current_page_item' :''}}><a href="/" accesskey="1" title="">Home</a></li>
    <li class="{{ Request::path()=== '/' ? 'current_page_item' :''}}><a href="/about" accesskey="1" title="">About</a></li>
</ul>
```

