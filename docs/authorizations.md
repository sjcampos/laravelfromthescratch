# Authorization

Authorization and authentication is not the same thing, authentication is knowing who is the user in our app, is like the user telling who he or she is, but autherization responds for what that user can or can't do in our app. We can manage this using tokens, middlewares and some other techniques to have access for the permissions of our users, we can see more information right [here](https://laravel.com/docs/8.x/authorization)

## Middleware

Laravel includes a middleware that can authorize actions before the incoming request even reaches your routes or controllers. By default, the Illuminate\Auth\Middleware\Authorize middleware is assigned the can key in your App\Http\Kernel class. We just need to add the middleware authorization to the route.
```
Route::get('/conversations/{conversation}',ConversationsController@show)->middleware('can:view,conversation');
```

