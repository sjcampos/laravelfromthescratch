# Database Access

Now we are going to see how to connect our [Laravel](https://www.laravel.com) app to a database

## Setup a database connection

If you want to set up a database connection the firs thing you have to do is use the .env file, there we can find the keys that are neccesary for making the connection

![Keys](./images/databasekeys.png "Keys")

As we can see we are going to need the type of database, host, port, name, user and password, all these keys are registered in the config folder where you will see a databasephp file, now you just look for the database that you wanna configure and start replacing those keys

The next step is to create a database or pick one that you already have, and lets configure the keys with our data

After we change the keys lets see how to connect

![SELECT](./images/Selectdata.png "Select")

With this code we are getting the first post that has the same name that we are giving as a parameter, but lets se how we can pass the data to a view so we can show it to users

If we use 
```
        return view('post',[
            'post' => $post
        ]);
```
we are sending the data to the view but if we want to show that data we just have to use this

```
{{ $post->name }}
```
'name' is the data that we want to show so you just have to replace it for the data that you need


## Eloquent

This is a cleaner way to access the database, it is a model of the table that we are going to access that allow us to be more efficient during this process
we just have to enter the next line in our console
```
php artisan make:model Post
```
Now we have a model where even we can write some code but we will talk about that later. The first thing you have to do is adding the use of that model to the controller
```
use App\Models\Posts;
```
Then we just have to change the way that we were using to access the database, and now we are going to use this

![SELECT](./images/selectcleaner.png "Select")

As you can see this is a cleaner way to connect to the database

## Migrations

This a very important topic because the most of the time you rarely create tables by hand or something like that, that is why we have the migrations, in [Laravel](https://laravel.com/) we just have to use the next command:
```
php artisan make:migration create_posts_table
```
Now we have a new class that we can edit

![Migration](./images/migration.png "Migration")

Once you are in that file you just have to add the keys or the data for your table just like this

![Keys](./images/AddKeys.png "Keys")

As you can see, you just need to add the data in the schema create, but as you can see we have 2 differents methods in our migration the method up and down, when you want yo run the migration you will use the method up but if you want to roll back you use the method down and in this method you can especify the actions that you want, for example delete the table, update the table,etc

And you just have to run this command and your migrations will be complete
```
php artisan migrate
```



