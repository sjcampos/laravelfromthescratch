# Eloquent

## Basic eloquent relationships

The are some basics relationships between model or objects for example we can have the next models:

User

Article

Image

An user can have a lot of articles and images and the articles and the article and image belongs to a user, Laravel give us a tool so we can get this data in a easier way.

Let's imagine that we are in the user model, and we need to get all of the articles
```
public function articles()
{
    return $this->hasMany(Article::class); //This is the same that select * from articles where userid = "id of the current user"
}
```
And in the article class we need this
```
public function user()
{
    return $this->belongsTo(User::class);
}
```
With this we just create a relationship between the user class and the article class that can be used in many ways, however, there are some other relationships like "hasOne", "belongstoMany" and of course "hasMany" and "belongsTo".

## Many to many relationships with linking tables

In a many to manu relationships we need to create a new table that allows us to combine the other 2 tables, for example we have a table image and a table tags, an image can have many tags and one tag can be use in many images so we create a table "image_tag" and we will save the image id and the tag id and lets see how to setup this
```
public function tags()
{
    return $this->belongsToMany(Tag::class);
}
```
And with that we have access to the all the tags in one image, if you want all the images associated to a tag you just have to use the same function but in the tag class.

## Attach many-to-many inserts

We can use the method attach to make the insertions en the database lets see how 
```
$article->tags()->attach(request('tags'));
```
Remember that article has the attribute tags so we are attaching all the tags selected by the user and making the insert in the database
