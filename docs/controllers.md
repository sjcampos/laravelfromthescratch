# Controllers Techniques

## Route model binding

In [Laravel](http://laravel.com) you can use a wildcard in the url as seen before and then make a query to the database, however, Laravel can perfom this query automatically for us by using the route model binding, lets see how we can use it

This is how we use the wildcard
```
public function  show($id)
{
    $article = Article::find($id);

    return view('articles.show', ['articles'=> $article]);    
}
```
But now we can use this
```
public function  show(Article $article)
{
    return view('articles.show', ['articles'=> $article]);    
}
```
Remember that the "Article" is a model that you can create and you are telling Laravel to find an article with the id that the user provide in the url

## Reduce duplication 

Laravel has a method that allows us the reduction of duplication for example we can use "validatedAttributes" that helps us to reduce the declaration of variables, lets see

![Reduce](./images/reduce.png "Reduce")

As we can see in the image the "validatedAttributes" in the request so we make sure that all the variables that are set as a requeried can be use by using "validatedAttributes", now if we want to create a new article we don't have to write down all the attributes for the object we just have to use "validatedAttributes" like this
```
Article::create($validatedAttributes);
```
And with that we assign all of the attributes without having to write all of then again. Another important thing that we have to remember is that it is important to pay attention and identify methods or code that is duplicated or that is used in many functions and if we find something it is advisable to move that code to a method that can be called in the different functions.



