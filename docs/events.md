# Events

This a very important topic and I think most of the most novice programmers often do not analyze all the events that can be unleashed with a simple action, for example making an online purchase, many times it is advisable to take a step back and carry out a thorough analysis of all the events that occur. are going to unleash or may become unleashed in the future for Laravel we can find more information right [here](https://laravel.com/docs/8.x/events)

Let's see how we can create an event and a listener

## Event 

Events  are use when we need to set like a condition, for example, if an user buys a product that will be our event and then that has to trigger a listener, we can create an event using the next command 
```
php artisan event:generate
```

## Listeners

Event listeners receive the event instance in their handle method. The event:generate command will automatically import the proper event class and type-hint the event on the handle method. listeners are use to create actions or register specific action after an event let's see an example:

Someone buys a product and you want to create a cupon or something like so, when the user buys the product our listener will be trigger by the event and will create the cupon.
