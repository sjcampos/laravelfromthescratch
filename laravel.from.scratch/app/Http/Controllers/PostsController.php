<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Posts;

class PostsController extends Controller
{
    public function show($name){

        $post = Posts::where('name', $name)->first();

        if(! $post){
            abort(404);
        }
    
        return view('post',[
            'post' => $post
        ]);
    }
}
