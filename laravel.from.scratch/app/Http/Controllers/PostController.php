<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PostController extends Controller
{
    public function show($post){
        $posts = [
            'first' => 'First data',
            'second' => 'Second data'
    
        ];
    
        if(! array_key_exists($post, $posts)){
            abort(404, "Sorry that post was no found");
        }
    
        return view('post',[
            'post' => $posts[$post]
        ]);
    }
}