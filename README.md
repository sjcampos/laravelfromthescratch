# LaravelFromTheScratch

This is a Laravel course that was done in the web applications class using free software.

These are the topics:

[Routing](./docs/routing.md)

[DataBase](./docs/database.md)

[Views](./docs/views.md)

[Forms](./docs/forms.md)

[Controllers Techniques](./docs/controllers.md)

[Eloquent](./docs/eloquent.md)

[Authentication](./docs/authentication.md)

[Core Concepts](./docs/coreconcepts.md)

[Mail](./docs/mail.md)

[Notifications](./docs/notifications.md)

[Events](./docs/events.md)

[Authorization](./docs/authorizations.md)